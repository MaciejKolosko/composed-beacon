﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">14044</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">9868950</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16448250</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">16448250</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)^!!!*Q(C=\&gt;5^&lt;B.2&amp;-8R!UJ"[RUA^[H/&amp;NR2)F&gt;5.,&gt;B!3ZI+,W&amp;+W5&amp;XI*\6^=&lt;)0)3]"&lt;#`TV&gt;BS#EO!'%5.\YT9T0P)^@XIQHUCRPJ"M^8#P(:QP8'?FYW2W@\/:H8"`('@&lt;*:;?_@OE[4,0BJ:'?\X`]K8](P]Z`R8_F[+7]F0_\@,H[)`DDPY.8'C]C;F'4'F2L40N1Z%6?Z%6?Z%7?Z%G?Z%G?Z%E?Z%%?Z%%?Z%&amp;O=J/&lt;X/1G.`H=S%5O=J&amp;$+C9P*CI',19I'E.2M3M]B;@Q&amp;"[_KP!5HM*4?!I0462Y#E`B+4S&amp;BWYK0)7H]"3?QM.15V*T)]&gt;4?"B?C3@R**\%EXC95IEH!334*1-HA]"1=D%Z34S**`&amp;QKM34?"*0YEE]8&amp;&lt;C34S**`%E(LL-6=GJ'2MZ(I:2Y!E]A3@Q""['6O!*0)%H]!1?JF0A#4Q")JAQ'"S#AEZ"A_",Y!E]("2Y!E`A#4S"BUPT$M6=G;%:'TE?YT%?YT%?YW%)'9`R')`R'!`$SHC-RXC-RXC93M:D0-:D)':3JJ=:T(1UD5RA0(TGU_*ZFX*+0$?J(V\V1[F_W.10E@LB5.^U^=V5XS4VYKM86&lt;V9[E61`X&amp;KN"KDHE4&gt;?445G@W*OK@OK&amp;PKBLKGLKB,[G*U`=U.T_?T4K?4^PO^&gt;LO&gt;NNON.JO.VOOV6KO6FMOF&amp;IP&amp;YWPA,&gt;PD#_((?_HAO`OPXT[^/XT]`*\DB]0NX@V@_P`]$\Q&lt;^6J0ZW#.PA-DG)J.!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="Messages" Type="Folder">
		<Item Name="Read Msg.lvclass" Type="LVClass" URL="../Messages/Read/Read Msg.lvclass"/>
		<Item Name="Write Msg.lvclass" Type="LVClass" URL="../Messages/Write/Write Msg.lvclass"/>
	</Item>
	<Item Name="Type" Type="Folder">
		<Item Name="composed-beacon peer.ctl" Type="VI" URL="../../../types/composed-beacon peer.ctl"/>
		<Item Name="UDP multicast service configuration.ctl" Type="VI" URL="../../../types/UDP multicast service configuration.ctl"/>
	</Item>
	<Item Name="UDP multicast service.lvclass" Type="LVClass" URL="../UDP multicast service.lvclass"/>
</Library>
